Class {
	#name : #BaselineOfOpenWhisk,
	#superclass : #BaselineOf,
	#category : #BaselineOfOpenWhisk
}

{ #category : #accessing }
BaselineOfOpenWhisk class >> defaultPackageNames [

	^ self packagesOfGroupNamed: #default
]

{ #category : #baseline }
BaselineOfOpenWhisk >> baseline: spec [

	<baseline>
	spec for: #common do: [ spec package: 'OpenWhisk' ]
]

{ #category : #accessing }
BaselineOfOpenWhisk >> repository [
	^ self packageRepositoryURL
]
