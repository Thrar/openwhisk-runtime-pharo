Class {
	#name : #OWRequest,
	#superclass : #Object,
	#instVars : [
		'json'
	],
	#category : #OpenWhisk
}

{ #category : #'instance creation' }
OWRequest class >> fromJSON: aJSON [

	"comment stating purpose of class-side method"

	"scope: class-variables  &  class-instance-variables"

	^ self new
		  json: aJSON;
		  yourself
]

{ #category : #'instance creation' }
OWRequest class >> fromString: aString [

	"comment stating purpose of class-side method"

	"scope: class-variables  &  class-instance-variables"

	^ self fromJSON: ((STONJSON fromString: aString) at: 'value')
]

{ #category : #accessing }
OWRequest >> json: aJSON [

	json := aJSON
]
