Class {
	#name : #OWRunRequest,
	#superclass : #OWRequest,
	#category : #OpenWhisk
}

{ #category : #'as yet unclassified' }
OWRunRequest class >> propagatedEnvironmentVariables [

	^ #( namespace action_name activation_id transaction_id
	     deadline api_key )
]

{ #category : #accessing }
OWRunRequest >> env [

	"comment stating purpose of instance-side method"

	"scope: class-variables  &  instance-variables"

	^ Dictionary
		  newFromKeys: (self class propagatedEnvironmentVariables collect: [ :var | 
				   '__OW_' , var uppercase ])
		  andValues:
		  (self class propagatedEnvironmentVariables collect: [ :var | json at: var ])
]

{ #category : #accessing }
OWRunRequest >> runArguments [

	^ json at: 'value'
]
