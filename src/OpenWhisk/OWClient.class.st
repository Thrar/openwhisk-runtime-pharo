Class {
	#name : #OWClient,
	#superclass : #Object,
	#category : #OpenWhisk
}

{ #category : #'as yet unclassified' }
OWClient class >> bundleAction: aClass [

	self bundleAction: aClass withDependencies: #()
]

{ #category : #'as yet unclassified' }
OWClient class >> bundleAction: aClass withDependencies: aCollectionOfClasses [

	| archive |
	archive := ZipArchive new.
	aClass fileOutToArchive: archive.
	aCollectionOfClasses do: [ :depclass | 
		depclass fileOutToArchive: archive ].
	archive writeToFile:
		FileSystem workingDirectory / ('ow-' , aClass name asFileName)
		, 'zip'
]
