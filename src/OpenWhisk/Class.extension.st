Extension { #name : #Class }

{ #category : #'*OpenWhisk' }
Class >> fileOutToArchive: anArchive [

	"comment stating purpose of instance-side method"

	"scope: class-variables  &  instance-variables"

	| defString |
	defString := String streamContents: [ :stream | 
		             self fileOutOn: stream ].
	anArchive addDeflateString: defString as: name asFileName , '.st'
]
