Class {
	#name : #OWInitRequest,
	#superclass : #OWRequest,
	#instVars : [
		'usercodes'
	],
	#category : #OpenWhisk
}

{ #category : #parsing }
OWInitRequest class >> readUserCode: aString isBinary: aBoolean [

	^ aBoolean
		  ifFalse: [ Collection with: aString ]
		  ifTrue: [ 
			  (ZipArchive new readFrom: aString base64Decoded readStream)
				  members collect: [ :member | member contents utf8Decoded ] ]
]

{ #category : #accessing }
OWInitRequest >> env [

	^ json at: 'env'
]

{ #category : #accessing }
OWInitRequest >> json: aJSON [

	"comment stating purpose of instance-side method"

	"scope: class-variables  &  instance-variables"

	super json: aJSON.
	self readUserCodes
]

{ #category : #accessing }
OWInitRequest >> main [

	^ json at: 'main'
]

{ #category : #parsing }
OWInitRequest >> readUserCodes [

	usercodes := self class
		             readUserCode: (json at: 'code')
		             isBinary: (json at: 'binary')
]

{ #category : #accessing }
OWInitRequest >> usercodes [

	^ usercodes
]
