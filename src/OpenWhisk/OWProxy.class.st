Class {
	#name : #OWProxy,
	#superclass : #Object,
	#instVars : [
		'usermain',
		'runEnv'
	],
	#category : #OpenWhisk
}

{ #category : #public }
OWProxy class >> startDefault [
	
	<script>
	self startInServerOn: 8080
]

{ #category : #public }
OWProxy class >> startInServerOn: port [

	"Start a new server bound to port on the local network running a REPL web service"

	"self startInServerOn: 1701"

	^ (ZnServer on: port)
		  bindingAddress: #[ 0 0 0 0 ];
		  delegate: self new;
		  start;
		  yourself
]

{ #category : #public }
OWProxy >> handleInit: aZnRequest [

	| req |
	req := OWInitRequest fromString: aZnRequest entity.
	usermain := req main.
	runEnv := req env.
	req usercodes do: [ :usercode | 
		CodeImporter evaluateString: usercode ].
	^ ZnResponse ok: (ZnEntity json: '{}')
]

{ #category : #public }
OWProxy >> handleRequest: request [

	request method = #POST ifFalse: [ 
		^ ZnResponse methodNotAllowed: request ].

	request uri firstPathSegment = #init ifTrue: [ 
		^ self handleInit: request ].
	request uri firstPathSegment = #run ifTrue: [ 
		^ self handleRun: request ].

	^ ZnResponse notFound: request uri
]

{ #category : #public }
OWProxy >> handleRun: aZnRequest [

	| req usermainret |
	req := OWRunRequest fromString: aZnRequest entity.
	Smalltalk os environment
		addAll: runEnv;
		addAll: req env.
	usermainret := (self class environment at: usermain asSymbol) main:
		               (req runArguments).
	^ ZnResponse ok: (ZnEntity json: (STONJSON toString: usermainret))
]
