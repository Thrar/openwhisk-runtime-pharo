Extension { #name : #OSEnvironment }

{ #category : #'*OpenWhisk' }
OSEnvironment >> addAll: aDictionary [

	aDictionary keysAndValuesDo: [ :key :value | self at: key put: value ]
]
