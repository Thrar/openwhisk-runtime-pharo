# Download Pharo Image in a clean image
FROM alpine:3 as download-image

RUN apk add unzip

WORKDIR /tmp/pharo-images

ADD https://files.pharo.org/image/100/Pharo10.0.0-0-metacello.build.512.sha.bfb3a61.arch.64bit.zip ./PharoImage.zip

RUN set -eu; \
  unzip PharoImage.zip; \
  rm PharoImage.zip; \
  mv *.image Pharo.image; \
  mv *.changes Pharo.changes; \
  true

# Copy Pharo Image into base image
FROM ghcr.io/ba-st/pharo-vm:v9.0.14 as image

WORKDIR /opt/pharo

USER root

RUN set -eu; \
  printf '#!/usr/bin/env bash\nexec pharo-vm Pharo.image "$@"' > /opt/pharo/pharo; \
  ln -s /opt/pharo/pharo /usr/local/bin/pharo; \
  chmod a+x /usr/local/bin/pharo; \
  chown 7431:100 /opt/pharo -R; \
  true

COPY --from=download-image --chown=pharo:users /tmp/pharo-images /opt/pharo/

RUN touch PharoV60.sources

RUN chmod a+w /opt/pharo

USER pharo

COPY . /tmp/openwhisk

RUN pharo metacello install "tonel:///tmp/openwhisk/src" BaselineOfOpenWhisk

ENTRYPOINT [ "pharo", "eval", "--no-quit", "OWProxy startDefault" ]
