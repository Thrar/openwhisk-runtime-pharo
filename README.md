# Apache OpenWhisk runtime for Pharo

Source code of the Pharo runtime for Apache OpenWhisk, mainly the code of the proxy implementing the Action Interface.

There is also helper code to work with Pharo actions.

## How to add the Pharo runtime to OpenWhisk

Ideally, you should follow all instructions in OpenWhisk's documentation to add a language runtime: [Adding Action Language Runtimes](https://github.com/apache/openwhisk/blob/master/docs/actions-new.md).
Read below for minimal steps.

Edit the runtime manifest ("ansible/files/runtimes.json") to add the runtime specification below to the dictionary of supported runtimes:

```json
"pharo": [
  {
      "kind": "pharo:10",
      "default": true,
      "image": {
          "prefix": "openwhisk",
          "name": "action-pharo-v10",
          "tag": "nightly"
      },
      "deprecated": false,
      "attached": {
          "attachmentName": "codefile",
          "attachmentType": "text/plain"
      },
      "requireMain": true
  }
]
```

Notes:

* Members of `image` must match the name of the Docker image of the Pharo runtime.

Then, redeploy OpenWhisk by running again the Ansible playbook "openwhisk.yml" as you are instructed by OpenWhisk's documentation for Ansible: [Deploying OpenWhisk using Ansible](https://github.com/apache/openwhisk/tree/master/ansible).
